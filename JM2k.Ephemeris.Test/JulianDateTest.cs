﻿using System;
using Microsoft.VisualStudio.TestPlatform.UnitTestFramework;

namespace JM2k.Ephemeris.Test
{
    [TestClass]
    public class JulianDateTest
    {
        [TestMethod]
        public void TestJ2000()
        {
            var dateTime = new DateTime(2000, 1, 1, 12, 0, 0);

            var jd = new JulianDate(dateTime);

            Assert.AreEqual(2451545, jd.Value);
        }

        [TestMethod]
        public void TestBirthday()
        {
            var dateTime = new DateTime(1982, 11, 22);

            var jd = new JulianDate(dateTime);

            Assert.AreEqual(2445295.5, jd.Value);
        }
    }
}
