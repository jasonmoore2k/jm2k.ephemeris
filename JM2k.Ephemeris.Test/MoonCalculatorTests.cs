﻿using System;
using Ephemeris.Test;
using Microsoft.VisualStudio.TestPlatform.UnitTestFramework;

namespace JM2k.Ephemeris.Test
{
    [TestClass]
    public class MoonCalculatorTests
    {
        [TestMethod]
        public void Test1()
        {
            var calculator = new MoonCalculator();
            var timezone = new TimeSpan(11, 0, 0); // AEDST
            var dateTime = new DateTimeOffset(2013, 12, 19, 08, 00, 00, timezone);

            var dist = calculator.GetMoonDistance(dateTime);
            Assert.AreEqual(405614.853, dist, 100.0);

            var raDec = calculator.GetRaDec(dateTime);
            Assert.AreEqual(103.922, raDec.RightAscension.ConvertToBetweenZeroAndTwoPi().Degrees, 1.0);
            Assert.AreEqual(18.092, raDec.Declination.ConvertToBetweenNegativePiAndPositivePi().Degrees, 1.0);

            var azAlt = calculator.GetAzAlt(dateTime, TestParameters.SydneyLatitude, TestParameters.SydneyLongitude);
            Assert.AreEqual(285.235, azAlt.Azimuth.ConvertToBetweenZeroAndTwoPi().Degrees, 1.0);
            Assert.AreEqual(-10.764, azAlt.Altitude.ConvertToBetweenNegativePiAndPositivePi().Degrees, 2.0);
        }

        [TestMethod]
        public void Test2()
        {
            var calculator = new MoonCalculator();
            var timezone = new TimeSpan(11, 0, 0); // AEDST
            var dateTime = new DateTimeOffset(2013, 12, 19, 08, 00, 00, timezone);

            var riseSetTimes = calculator.GetRiseSetTimes(dateTime, TestParameters.SydneyLatitude, TestParameters.SydneyLongitude);

            //Assert.AreEqual(06 * 60 + 10, riseSetTimes.RiseTime.TotalMinutes, 1); // 06:10 am
            //Assert.AreEqual(20 * 60 + 05, riseSetTimes.SetTime.TotalMinutes, 1);  // 20:05 pm
        }
    }
}