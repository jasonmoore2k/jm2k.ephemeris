﻿using JM2k.Ephemeris;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace Ephemeris.Test
{
    [TestClass]
    public class SunCalculatorTests
    {
        [TestMethod]
        public void Test1()
        {
            var calculator = new SunCalculator();
            var timezone = new TimeSpan(11, 0, 0); // AEDST
            var dateTime = new DateTimeOffset(2014, 01, 26, 18, 00, 00, timezone);

            var raDec = calculator.GetRaDec(dateTime);
            Assert.AreEqual(-18.7, raDec.Declination.ConvertToBetweenNegativePiAndPositivePi().Degrees, 0.1);

            var azAlt = calculator.GetAzAlt(dateTime, TestParameters.SydneyLatitude, TestParameters.SydneyLongitude);
            Assert.AreEqual(262.93, azAlt.Azimuth.ConvertToBetweenZeroAndTwoPi().Degrees, 0.1);
            Assert.AreEqual(24.12, azAlt.Altitude.ConvertToBetweenNegativePiAndPositivePi().Degrees, 0.1);
        }

        [TestMethod]
        public void Test2()
        {
            var calculator = new SunCalculator();
            var timezone = new TimeSpan(11, 0, 0); // AEDST
            var dateTime = new DateTimeOffset(2014, 01, 26, 18, 00, 00, timezone);

            var riseSetTimes = calculator.GetRiseSetTimes(dateTime, TestParameters.SydneyLatitude, TestParameters.SydneyLongitude);

            Assert.AreEqual(06*60 + 10, riseSetTimes.RiseTime.TotalMinutes, 1); // 06:10 am
            Assert.AreEqual(20*60 + 05, riseSetTimes.SetTime.TotalMinutes, 1);  // 20:05 pm
        }
    }
}