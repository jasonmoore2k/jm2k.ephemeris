﻿using JM2k.Ephemeris;
using System;

namespace Ephemeris.Test
{
    public static class TestParameters
    {
        public static Angle SydneyLatitude = new Angle(-33.88, Angle.AngleUnits.Degrees);
        public static Angle SydneyLongitude = new Angle(151.17, Angle.AngleUnits.Degrees);
    }
}