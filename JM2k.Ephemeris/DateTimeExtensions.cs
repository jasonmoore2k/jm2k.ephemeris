﻿using System;

namespace JM2k.Ephemeris
{
    public static class DateTimeExtensions
    {
        public static DateTime? ToLocalTime(this DateTime? nullableDateTime)
        {
            return nullableDateTime?.ToLocalTime();
        }

        public static DateTime? ToUniversalTime(this DateTime? nullableDateTime)
        {
            return nullableDateTime?.ToUniversalTime();
        }

        // This method is not found in Portable.
        public static double ToOADate(this DateTime dateTime)
        {
            const long ticks18991230 = 599264352000000000L;
            const double OAMinValue = -657435.0d;
            const double OAMaxValue = 2958466.0d;

            long t = dateTime.Ticks;
            // uninitialized DateTime case
            if (t == 0)
                return 0;
            // we can't reach minimum value
            if (t < 31242239136000000)
                return OAMinValue + 0.001;

            TimeSpan ts = new TimeSpan(dateTime.Ticks - ticks18991230);
            double result = ts.TotalDays;
            // t < 0 (where 599264352000000000 == 0.0d for OA)
            if (t < 599264352000000000)
            {
                // negative days (int) but decimals are positive
                double d = Math.Ceiling(result);
                result = d - 2 - (result - d);
            }
            else
            {
                // we can't reach maximum value
                if (result >= OAMaxValue)
                    result = OAMaxValue - 0.00000001d;
            }
            return result;
        }
    }
}