﻿using System;

namespace JM2k.Ephemeris
{
    public class AzAlt
    {
        private readonly Tuple<Angle, Angle> _point;

        public Angle Azimuth => _point.Item1;

        public Angle Altitude => _point.Item2;

        public AzAlt(Angle az, Angle alt)
        {
            _point = Tuple.Create(az, alt);
        }

        public override int GetHashCode()
        {
            return _point.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            var azAlt = obj as AzAlt;
            if (azAlt == null)
                return false;

            return _point.Equals(azAlt._point);
        }

        public AzAlt ConvertTo(Angle.AngleUnits units)
        {
            return new AzAlt(Azimuth.ConvertTo(units), Altitude.ConvertTo(units));
        }
    }
}