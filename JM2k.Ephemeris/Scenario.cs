﻿using System;
using System.Runtime.Serialization;

namespace JM2k.Ephemeris
{
    [DataContract]
    public class Scenario
    {
        private Guid? _uid;

        [DataMember]
        public Guid Uid
        {
            get { return _uid ?? Guid.NewGuid(); }
            set { _uid = value; }
        }

        [DataMember]
        public string Name { get; set; }

        public DateTimeOffset DateTime { get; set; }

        [DataMember]
        public long DateTimeTicks
        {
            get { return DateTime.Ticks; }
            set { DateTime = new DateTimeOffset(value, DateTime.Offset); }
        }

        [DataMember]
        public TimeSpan OffsetPart
        {
            get { return DateTime.Offset; }
            set { DateTime = new DateTimeOffset(DateTime.DateTime, value); }
        }

        [DataMember]
        public string TimeZone { get; set; }

        public LatLong Location { get; set; }

        [DataMember]
        public double LatitudeDegrees
        {
            get { return Location.Latitude.Degrees; }
            set
            {
                Location = new LatLong(
                    Angle.FromDegrees(value),
                    Location != null ? Location.Longitude : Angle.Zero);
            }
        }

        [DataMember]
        public double LongitudeDegrees
        {
            get { return Location.Longitude.Degrees; }
            set
            {
                Location = new LatLong(
                    Location != null ? Location.Latitude : Angle.Zero,
                    Angle.FromDegrees(value));
            }
        }
    }
}