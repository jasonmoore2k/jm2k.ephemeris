﻿using System;

namespace JM2k.Ephemeris
{
    public static class PosMath
    {
        private static readonly Angle EarthObliquity = new Angle(23.4397, Angle.AngleUnits.Degrees);

        public static Angle GetRightAscension(Angle l, Angle b)
        {
            return Atan2(
                Math.Sin(l.Radians) * Math.Cos(EarthObliquity.Radians) -
                Math.Tan(b.Radians) * Math.Sin(EarthObliquity.Radians),
                Math.Cos(l.Radians));
        }

        public static Angle GetDeclination(Angle l, Angle b)
        {
            return Asin(
                Math.Sin(b.Radians) * Math.Cos(EarthObliquity.Radians) +
                Math.Cos(b.Radians) * Math.Sin(EarthObliquity.Radians) * Math.Sin(l.Radians));
        }

        public static Angle GetAzimuth(Angle ha, Angle phi, Angle dec)
        {
            var value = Atan2(
                Math.Sin(ha.Radians),
                Math.Cos(ha.Radians) * Math.Sin(phi.Radians) -
                Math.Tan(dec.Radians) * Math.Cos(phi.Radians));

            // Add 180 degrees to set the origin (0 degrees) to be North.
            return value + Angle.PiRadians;
        }

        public static Angle GetAltitude(Angle ha, Angle phi, Angle dec)
        {
            return Asin(
                Math.Sin(phi.Radians) * Math.Sin(dec.Radians) +
                Math.Cos(phi.Radians) * Math.Cos(dec.Radians) * Math.Cos(ha.Radians));
        }

        public static Angle GetSiderealTime(JulianDate jd, Angle longtitude)
        {
            const double earthTheta0 = 280.16;
            const double earthTheta1 = 360.9856235;
            var value = new Angle(earthTheta0 + earthTheta1 * jd.Days, Angle.AngleUnits.Degrees);
            return value + longtitude;
        }

        public static Angle GetMeanAnomalyOfEarth(JulianDate jd)
        {
            var t = jd.Days / 36525.0;
            var result = 357.52911 + t * (35999.05029 - 0.0001537 * t);
            return new Angle(result, Angle.AngleUnits.Degrees);
        }

        public static Angle Asin(double d)
        {
            return new Angle(Math.Asin(d), Angle.AngleUnits.Radians);
        }

        public static Angle Acos(double d)
        {
            return new Angle(Math.Acos(d), Angle.AngleUnits.Radians);
        }

        public static Angle Atan(double d)
        {
            return new Angle(Math.Atan(d), Angle.AngleUnits.Radians);
        }

        public static Angle Atan2(double y, double x)
        {
            return new Angle(Math.Atan2(y, x), Angle.AngleUnits.Radians);
        }
    }
}