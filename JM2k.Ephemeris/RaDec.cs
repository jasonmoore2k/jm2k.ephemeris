﻿using System;

namespace JM2k.Ephemeris
{
    public class RaDec
    {
        private readonly Tuple<Angle, Angle> _point;

        public Angle RightAscension => _point.Item1;

        public Angle Declination => _point.Item2;

        public RaDec(Angle ra, Angle dec)
        {
            _point = Tuple.Create(ra, dec);
        }

        public override int GetHashCode()
        {
            return _point.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            var raDec = obj as RaDec;
            if (raDec == null)
                return false;

            return _point.Equals(raDec._point);
        }

        public AzAlt ConvertToAzAlt(DateTimeOffset dateTime, Angle latitude, Angle longitude)
        {
            var jd = dateTime.ToJulianDate();
            var ha = PosMath.GetSiderealTime(jd, longitude) - RightAscension;

            return new AzAlt(
                PosMath.GetAzimuth(ha, latitude, Declination),
                PosMath.GetAltitude(ha, latitude, Declination)
            );
        }
    }
}