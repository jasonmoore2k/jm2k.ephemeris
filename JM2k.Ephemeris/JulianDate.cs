﻿using System;

namespace JM2k.Ephemeris
{
    public class JulianDate
    {
        public const int J2000 = 2451545;

        public double Value { get; private set; }

        public JulianDate(double value)
        {
            Value = value;
        }

        public JulianDate(DateTime date)
        {
            // The Julian Date of midnight, December 30, 1899 is 2415018.5
            Value = date.ToOADate() + 2415018.5;
        }

        public double Days => Value - J2000;
    }

    public static class JulianDateExtensions
    {
        public static JulianDate ToJulianDate(this DateTime dateTime)
        {
            return new JulianDate(dateTime);
        }

        public static JulianDate ToJulianDate(this DateTimeOffset dateTime)
        {
            return new JulianDate(dateTime.UtcDateTime);
        }
    }
}