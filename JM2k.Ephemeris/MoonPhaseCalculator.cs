﻿using System;

namespace JM2k.Ephemeris
{
    public interface IMoonPhaseCalculator
    {
        double GetMoonFraction(DateTimeOffset dateTime);
    }

    // http://idlastro.gsfc.nasa.gov/ftp/pro/astro/mphase.pro
    public class MoonPhaseCalculator : IMoonPhaseCalculator
    {
        private readonly ISunCalculator _sunCalculator;
        private readonly IMoonCalculator _moonCalculator;

        public MoonPhaseCalculator(
            ISunCalculator sunCalculator,
            IMoonCalculator moonCalculator)
        {
            _sunCalculator = sunCalculator;
            _moonCalculator = moonCalculator;
        }

        public double GetMoonFraction(DateTimeOffset dateTime)
        {
            var sun = _sunCalculator.GetRaDec(dateTime);
            var moon = _moonCalculator.GetRaDec(dateTime);
            var moonDist = _moonCalculator.GetMoonDistance(dateTime);
            const double sunDist = 149598000; // distance from Earth to Sun in km

            var phi = PosMath.Acos(sun.Declination.Sin() * moon.Declination.Sin() + sun.Declination.Cos() * moon.Declination.Cos() * (sun.RightAscension - moon.RightAscension).Cos());
            var inc = PosMath.Atan2(sunDist * phi.Sin(), moonDist - sunDist * phi.Cos());

            return (1.0 + inc.Cos()) / 2.0;
        }
    }
}