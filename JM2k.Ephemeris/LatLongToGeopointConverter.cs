﻿using System;
using Windows.Devices.Geolocation;
using Windows.UI.Xaml.Data;

namespace JM2k.Ephemeris
{
    public class LatLongToGeopointConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            return ConvertLocation(value as LatLong);
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            return ConvertLocation(value as Geopoint);
        }

        public Geopoint ConvertLocation(LatLong location)
        {
            if (location == null)
                return null;
            return new Geopoint(
                new BasicGeoposition {
                    Altitude = 0,
                    Latitude = location.Latitude.Degrees,
                    Longitude = location.Longitude.Degrees
                },
                AltitudeReferenceSystem.Unspecified);
        }

        public LatLong ConvertLocation(Geopoint location)
        {
            if (location == null)
                return null;
            return new LatLong(
                new Angle(location.Position.Latitude, Angle.AngleUnits.Degrees),
                new Angle(location.Position.Longitude, Angle.AngleUnits.Degrees));
        }
    }
}