﻿using System;

namespace JM2k.Ephemeris
{
    public class RiseSetTimes
    {
        public TimeSpan RiseTime { get; private set; }

        public TimeSpan SetTime { get; private set; }

        public RiseSetTimes(TimeSpan riseTime, TimeSpan setTime)
        {
            RiseTime = riseTime;
            SetTime = setTime;
        }
    }
}