﻿using System;

namespace JM2k.Ephemeris
{
    public class Angle
    {
        public static readonly Angle Zero = new Angle(0, AngleUnits.Radians);
        public static readonly Angle PiRadians = new Angle(Math.PI, AngleUnits.Radians);
        public static readonly Angle TwoPiRadians = new Angle(2 * Math.PI, AngleUnits.Radians);
        public static readonly Angle RightAngle = new Angle(90, AngleUnits.Degrees);

        public double Value { get; private set; }

        public AngleUnits Units { get; private set; }

        public double Degrees => ConvertToDegrees();

        public double Radians => ConvertToRadians();

        public double Hours => ConvertToHours();

        public Angle(double value, AngleUnits units)
        {
            Value = value;
            Units = units;
        }

        public Angle(TimeSpan timespan)
            : this(timespan.TotalHours, AngleUnits.Hours)
        {
        }

        public Angle ConvertTo(AngleUnits units)
        {
            if (units == Units)
            {
                return this;
            }

            double value;
            switch (units)
            {
                case AngleUnits.Degrees:
                    value = ConvertToDegrees();
                    break;

                case AngleUnits.Radians:
                    value = ConvertToRadians();
                    break;

                case AngleUnits.Hours:
                    value = ConvertToDegrees() / 15.0;
                    break;

                default:
                    throw new ArgumentOutOfRangeException(nameof(units));
            }

            return new Angle(value, units);
        }

        public Angle ConvertToBetweenZeroAndTwoPi()
        {
            bool invert = false;
            var value = Value;
            if (Value < 0)
            {
                value = -value;
                invert = true;
            }

            var fullAngle = TwoPiRadians.ConvertTo(Units).Value;
            value = value % fullAngle;

            if (invert)
            {
                value = fullAngle - value;
            }

            return new Angle(value, Units);
        }

        public Angle ConvertToBetweenNegativePiAndPositivePi()
        {
            return (this + PiRadians).ConvertToBetweenZeroAndTwoPi() - PiRadians;
        }

        private double ConvertToDegrees()
        {
            switch (Units)
            {
                case AngleUnits.Degrees:
                    return Value;

                case AngleUnits.Radians:
                    return Value / Math.PI * 180.0;

                case AngleUnits.Hours:
                    return Value * 15;

                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private double ConvertToRadians()
        {
            switch (Units)
            {
                case AngleUnits.Radians:
                    return Value;

                case AngleUnits.Degrees:
                    return Value / 180.0 * Math.PI;

                case AngleUnits.Hours:
                    return Value / 12.0 * Math.PI;

                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private double ConvertToHours()
        {
            switch (Units)
            {
                case AngleUnits.Degrees:
                    return Value / 15;

                case AngleUnits.Radians:
                    return Value * 12 / Math.PI;

                case AngleUnits.Hours:
                    return Value;

                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        public static Angle FromDegrees(double value)
        {
            return new Angle(value, AngleUnits.Degrees);
        }

        public static Angle FromRadians(double value)
        {
            return new Angle(value, AngleUnits.Radians);
        }

        public static Angle FromHours(double value)
        {
            return new Angle(value, AngleUnits.Hours);
        }

        public static Angle operator *(Angle angle, double rhs)
        {
            return new Angle(angle.Value * rhs, angle.Units);
        }

        public static Angle operator *(double lhs, Angle angle)
        {
            return angle * lhs;
        }

        public static Angle operator /(Angle angle, double rhs)
        {
            return new Angle(angle.Value / rhs, angle.Units);
        }

        public static double operator /(Angle a1, Angle a2)
        {
            return a1.Value / a2.ConvertTo(a1.Units).Value;
        }

        public static Angle operator +(Angle a1, Angle a2)
        {
            return new Angle(a1.Value + a2.ConvertTo(a1.Units).Value, a1.Units);
        }

        public static Angle operator -(Angle a1, Angle a2)
        {
            return new Angle(a1.Value - a2.ConvertTo(a1.Units).Value, a1.Units);
        }

        public double Sin()
        {
            return Math.Sin(Radians);
        }

        public double Cos()
        {
            return Math.Cos(Radians);
        }

        public double Tan()
        {
            return Math.Tan(Radians);
        }

        public enum AngleUnits
        {
            Degrees,
            Radians,
            Hours,
        }

        public override string ToString()
        {
            switch (Units)
            {
                case AngleUnits.Degrees:
                    return $"{Value:F2}°";

                case AngleUnits.Radians:
                    return $"{Value:F2} rad";

                case AngleUnits.Hours:
                    var hours = (int)Value;
                    var minutes = (int)((Value - hours) / 60);
                    return $"{hours}/:{minutes}";

                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        public override bool Equals(object obj)
        {
            var other = obj as Angle;
            if (other == null)
                return false;

            return Math.Abs(Value - other.ConvertTo(Units).Value) <= Double.Epsilon;
        }
    }
}