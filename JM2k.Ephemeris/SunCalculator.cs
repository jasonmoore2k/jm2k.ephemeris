﻿using System;

namespace JM2k.Ephemeris
{
    public interface ISunCalculator
    {
        RaDec GetRaDec(DateTimeOffset dateTime);

        AzAlt GetAzAlt(DateTimeOffset dateTime, Angle latitude, Angle longitude);

        RiseSetTimes GetRiseSetTimes(DateTimeOffset dateTime, Angle latitude, Angle longitude);

        RiseSetTimes GetCivilTwilightTimes(DateTimeOffset dateTime, Angle latitude, Angle longitude);

        RiseSetTimes GetNauticalTwilightTimes(DateTimeOffset dateTime, Angle latitude, Angle longitude);

        RiseSetTimes GetAstronomicalTwilightTimes(DateTimeOffset dateTime, Angle latitude, Angle longitude);

        RiseSetTimes GetGoldenHourTimes(DateTimeOffset dateTime, Angle latitude, Angle longitude);
    }

    // http://www.esrl.noaa.gov/gmd/grad/solcalc/main.js
    public class SunCalculator : ISunCalculator
    {
        private static readonly Angle SunRiseSetHeight = new Angle(-0.833, Angle.AngleUnits.Degrees);
        private static readonly Angle CivilTwilightHeight = new Angle(-6, Angle.AngleUnits.Degrees);
        private static readonly Angle NauticalTwilightHeight = new Angle(-12, Angle.AngleUnits.Degrees);
        private static readonly Angle AstronomicalTwilightHeight = new Angle(-18, Angle.AngleUnits.Degrees);
        private static readonly Angle GoldenHour = new Angle(10, Angle.AngleUnits.Degrees);

        public RaDec GetRaDec(DateTimeOffset dateTime)
        {
            var jd = dateTime.ToJulianDate();

            var e = CalcObliquityCorrection(jd);
            var lambda = CalcSunApparentLong(jd);
            var tananum = e.Cos() * lambda.Sin();
            var tanadenom = lambda.Cos();
            var sint = e.Sin() * lambda.Sin();

            var rightAscension = new Angle(Math.Atan2(tananum, tanadenom), Angle.AngleUnits.Radians);
            var declination = new Angle(Math.Asin(sint), Angle.AngleUnits.Radians);

            return new RaDec(rightAscension, declination);
        }

        public AzAlt GetAzAlt(DateTimeOffset dateTime, Angle latitude, Angle longitude)
        {
            var jd = dateTime.Date.ToJulianDate();
            var timezone = new Angle(dateTime.Offset.TotalHours, Angle.AngleUnits.Hours);
            var localTime = new Angle(dateTime.TimeOfDay);

            var eqTime = CalcEquationOfTime(jd);
            var theta = CalcSunDeclination(jd);

            var solarTimeFix = eqTime + longitude - timezone;
            var trueSolarTime = localTime + solarTimeFix;
            trueSolarTime = trueSolarTime.ConvertToBetweenZeroAndTwoPi();
            var hourAngle = trueSolarTime - Angle.PiRadians;
            hourAngle = hourAngle.ConvertToBetweenNegativePiAndPositivePi();

            var csz = latitude.Sin() * theta.Sin() + latitude.Cos() * theta.Cos() * hourAngle.Cos();
            if (csz > 1.0)
            {
                csz = 1.0;
            }
            else if (csz < -1.0)
            {
                csz = -1.0;
            }
            var zenith = PosMath.Acos(csz);
            var azDenom = latitude.Cos() * zenith.Sin();
            Angle azimuth;
            if (Math.Abs(azDenom) > 0.001)
            {
                var azRad = (latitude.Sin() * zenith.Cos() - theta.Sin()) / azDenom;
                if (Math.Abs(azRad) > 1.0)
                {
                    if (azRad < 0)
                    {
                        azRad = -1.0;
                    }
                    else
                    {
                        azRad = 1.0;
                    }
                }
                azimuth = Angle.PiRadians - PosMath.Acos(azRad);
                if (hourAngle.Value > 0.0)
                {
                    azimuth = -1 * azimuth;
                }
            }
            else
            {
                if (latitude.Value > 0.0)
                {
                    azimuth = Angle.PiRadians;
                }
                else
                {
                    azimuth = Angle.Zero;
                }
            }
            if (azimuth.Value < 0.0)
            {
                azimuth += Angle.TwoPiRadians;
            }
            var exoatmElevation = new Angle(90, Angle.AngleUnits.Degrees) - zenith;

            // Atmospheric Refraction correction

            var refractionCorrection = 0.0;
            if (exoatmElevation.Degrees <= 85.0)
            {
                var te = exoatmElevation.Tan();
                if (exoatmElevation.Degrees > 5.0)
                {
                    refractionCorrection = 58.1 / te - 0.07 / (te * te * te) + 0.000086 / (te * te * te * te * te);
                }
                else if (exoatmElevation.Degrees > -0.575)
                {
                    refractionCorrection = 1735.0 + exoatmElevation.Degrees * (-518.2 + exoatmElevation.Degrees * (103.4 + exoatmElevation.Degrees * (-12.79 + exoatmElevation.Degrees * 0.711)));
                }
                else
                {
                    refractionCorrection = -20.774 / te;
                }
                refractionCorrection = refractionCorrection / 3600.0;
            }

            var solarZenith = zenith - new Angle(refractionCorrection, Angle.AngleUnits.Degrees);
            var altitude = Angle.RightAngle - solarZenith;

            return new AzAlt(azimuth, altitude);
        }

        public RiseSetTimes GetRiseSetTimes(DateTimeOffset dateTime, Angle latitude, Angle longitude)
        {
            return GetRiseSetTimes(dateTime, latitude, longitude, SunRiseSetHeight);
        }

        public RiseSetTimes GetCivilTwilightTimes(DateTimeOffset dateTime, Angle latitude, Angle longitude)
        {
            return GetRiseSetTimes(dateTime, latitude, longitude, CivilTwilightHeight);
        }

        public RiseSetTimes GetNauticalTwilightTimes(DateTimeOffset dateTime, Angle latitude, Angle longitude)
        {
            return GetRiseSetTimes(dateTime, latitude, longitude, NauticalTwilightHeight);
        }

        public RiseSetTimes GetAstronomicalTwilightTimes(DateTimeOffset dateTime, Angle latitude, Angle longitude)
        {
            return GetRiseSetTimes(dateTime, latitude, longitude, AstronomicalTwilightHeight);
        }

        public RiseSetTimes GetGoldenHourTimes(DateTimeOffset dateTime, Angle latitude, Angle longitude)
        {
            return GetRiseSetTimes(dateTime, latitude, longitude, GoldenHour);
        }

        private RiseSetTimes GetRiseSetTimes(DateTimeOffset dateTime, Angle latitude, Angle longitude, Angle height)
        {
            var jd = dateTime.Date.ToJulianDate();
            var eqTime = CalcEquationOfTime(jd);
            var solarDec = CalcSunDeclination(jd);
            var hourAngle = CalcHourAngleSunrise(latitude, solarDec, height);

            var sunriseDelta = longitude + hourAngle;
            var sunriseTimeAngle = Angle.PiRadians - sunriseDelta - eqTime;
            var sunrise = new TimeSpan((long)Math.Round(sunriseTimeAngle.Hours * TimeSpan.TicksPerHour));

            var sunsetDelta = longitude - hourAngle;
            var sunsetTimeAngle = Angle.PiRadians - sunsetDelta - eqTime;
            var sunset = new TimeSpan((long)Math.Round(sunsetTimeAngle.Hours * TimeSpan.TicksPerHour));

            // Apply time offset.
            sunrise = sunrise.Add(dateTime.Offset);
            sunset = sunset.Add(dateTime.Offset);

            return new RiseSetTimes(sunrise, sunset);
        }

        private double CalcTimeJulianCent(JulianDate jd)
        {
            return jd.Days / 36525.0;
        }

        private Angle CalcGeomMeanLongSun(JulianDate jd)
        {
            var t = CalcTimeJulianCent(jd);
            var l0 = 280.46646 + t * (36000.76983 + t * (0.0003032));
            while (l0 > 360.0)
            {
                l0 -= 360.0;
            }
            while (l0 < 0.0)
            {
                l0 += 360.0;
            }
            return new Angle(l0, Angle.AngleUnits.Degrees);
        }

        private Angle CalcGeomMeanAnomalySun(JulianDate jd)
        {
            var t = CalcTimeJulianCent(jd);
            var result = 357.52911 + t * (35999.05029 - 0.0001537 * t);
            return new Angle(result, Angle.AngleUnits.Degrees);
        }

        private double CalcEccentricityEarthOrbit(JulianDate jd)
        {
            var t = CalcTimeJulianCent(jd);
            return 0.016708634 - t * (0.000042037 + 0.0000001267 * t);
        }

        private Angle CalcSunEqOfCenter(JulianDate jd)
        {
            var t = CalcTimeJulianCent(jd);
            var ma = CalcGeomMeanAnomalySun(jd);

            var sinm = ma.Sin();
            var sin2m = (2 * ma).Sin();
            var sin3m = (3 * ma).Sin();

            var result = sinm * (1.914602 - t * (0.004817 + 0.000014 * t)) + sin2m * (0.019993 - 0.000101 * t) + sin3m * 0.000289;
            return new Angle(result, Angle.AngleUnits.Degrees);
        }

        private Angle CalcSunTrueLong(JulianDate jd)
        {
            var l0 = CalcGeomMeanLongSun(jd);
            var c = CalcSunEqOfCenter(jd);
            return l0 + c;
        }

        private Angle CalcSunApparentLong(JulianDate jd)
        {
            var t = CalcTimeJulianCent(jd);
            var o = CalcSunTrueLong(jd);
            var omega = new Angle(125.04 - 1934.136 * t, Angle.AngleUnits.Degrees);
            return o - new Angle(0.00569 - 0.00478 * omega.Sin(), Angle.AngleUnits.Degrees);
        }

        private Angle CalcMeanObliquityOfEcliptic(JulianDate jd)
        {
            var t = CalcTimeJulianCent(jd);
            var seconds = 21.448 - t * (46.8150 + t * (0.00059 - t * 0.001813));
            var e0 = 23.0 + (26.0 + (seconds / 60.0)) / 60.0;
            return new Angle(e0, Angle.AngleUnits.Degrees);
        }

        private Angle CalcObliquityCorrection(JulianDate jd)
        {
            var t = CalcTimeJulianCent(jd);
            var e0 = CalcMeanObliquityOfEcliptic(jd);
            var omega = new Angle(125.04 - 1934.136 * t, Angle.AngleUnits.Degrees);
            return e0 + new Angle(0.00256 * omega.Cos(), Angle.AngleUnits.Degrees);
        }

        public Angle CalcSunDeclination(JulianDate jd)
        {
            var e = CalcObliquityCorrection(jd);
            var lambda = CalcSunApparentLong(jd);
            var sint = e.Sin() * lambda.Sin();
            var result = Math.Asin(sint);
            return new Angle(result, Angle.AngleUnits.Radians);
        }

        private Angle CalcEquationOfTime(JulianDate jd)
        {
            var epsilon = CalcObliquityCorrection(jd);
            var l0 = CalcGeomMeanLongSun(jd);
            var e = CalcEccentricityEarthOrbit(jd);
            var m = CalcGeomMeanAnomalySun(jd);

            var y = (epsilon / 2).Tan();
            y *= y;

            var sin2l0 = (2 * l0).Sin();
            var sinm = m.Sin();
            var cos2l0 = (2 * l0).Cos();
            var sin4l0 = (4 * l0).Sin();
            var sin2m = (2 * m).Sin();

            var result = y * sin2l0 - 2.0 * e * sinm + 4.0 * e * y * sinm * cos2l0 - 0.5 * y * y * sin4l0 - 1.25 * e * e * sin2m;
            return new Angle(result, Angle.AngleUnits.Radians);
        }

        private Angle CalcHourAngleSunrise(Angle latitude, Angle solarDec, Angle height)
        {
            var fromZen = Angle.RightAngle - height;
            var arg = fromZen.Cos() / (latitude.Cos() * solarDec.Cos()) - latitude.Tan() * solarDec.Tan();
            var result = Math.Acos(arg);
            return new Angle(result, Angle.AngleUnits.Radians);
        }
    }
}