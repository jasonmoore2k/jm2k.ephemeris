﻿using System;

namespace JM2k.Ephemeris
{
    public interface IMoonCalculator
    {
        RaDec GetRaDec(DateTimeOffset dateTime);

        AzAlt GetAzAlt(DateTimeOffset dateTime, Angle latitude, Angle longitude);

        RiseSetTimes GetRiseSetTimes(DateTimeOffset dateTime, Angle latitude, Angle longitude);

        double GetMoonDistance(DateTimeOffset dateTime);
    }

    // http://aa.quae.nl/en/reken/hemelpositie.html
    public class MoonCalculator : IMoonCalculator
    {
        private static readonly Angle L0 = new Angle(218.316, Angle.AngleUnits.Degrees);
        private static readonly Angle L1 = new Angle(13.176396, Angle.AngleUnits.Degrees);
        private static readonly Angle L2 = new Angle(6.289, Angle.AngleUnits.Degrees);
        private static readonly Angle M0 = new Angle(134.963, Angle.AngleUnits.Degrees);
        private static readonly Angle M1 = new Angle(13.0649929509, Angle.AngleUnits.Degrees);
        private static readonly Angle F0 = new Angle(93.272, Angle.AngleUnits.Degrees);
        private static readonly Angle F1 = new Angle(13.229350, Angle.AngleUnits.Degrees);
        private static readonly Angle B0 = new Angle(5.128, Angle.AngleUnits.Degrees);
        private static readonly Angle R0 = new Angle(0.017, Angle.AngleUnits.Degrees);
        private static readonly Angle R1 = new Angle(10.26, Angle.AngleUnits.Degrees);
        private static readonly Angle R2 = new Angle(5.10, Angle.AngleUnits.Degrees);

        public RaDec GetRaDec(DateTimeOffset dateTime)
        {
            var jd = dateTime.ToJulianDate();
            return GetRaDec(jd);
        }

        private RaDec GetRaDec(JulianDate jd)
        {
            // Longtitude
            var l = L0 + L1 * jd.Days; // ecliptic longtitude
            l = l + L2 * GetMeanAnomaly(jd).Sin();

            // Latitude
            var f = F0 + F1 * jd.Days;
            var b = B0 * f.Sin();

            return new RaDec(
                PosMath.GetRightAscension(l, b),
                PosMath.GetDeclination(l, b)
                );
        }

        public AzAlt GetAzAlt(DateTimeOffset dateTime, Angle latitude, Angle longitude)
        {
            var raDec = GetRaDec(dateTime);
            var azAlt = raDec.ConvertToAzAlt(dateTime, latitude, longitude);
            return CorrectForRefraction(azAlt);
        }

        public RiseSetTimes GetRiseSetTimes(DateTimeOffset dateTime, Angle latitude, Angle longitude)
        {
            var jd = dateTime.ToJulianDate();
            var ma = PosMath.GetMeanAnomalyOfEarth(jd);
            var raDec = GetRaDec(jd);
            var h0 = Angle.Zero;

            var transit = raDec.RightAscension - latitude - ma + new Angle(new TimeSpan(17, 08, 15));
            var height = PosMath.Acos(
                (h0.Sin() - latitude.Sin() * raDec.Declination.Sin()) /
                (latitude.Cos() * raDec.Declination.Cos()));

            var moonrise = new TimeSpan((long)Math.Round((transit - height).ConvertToBetweenZeroAndTwoPi().Hours * TimeSpan.TicksPerHour));
            var moonset = new TimeSpan((long)Math.Round((transit + height).ConvertToBetweenZeroAndTwoPi().Hours * TimeSpan.TicksPerHour));

            return new RiseSetTimes(moonrise, moonset);
        }

        public double GetMoonDistance(DateTimeOffset dateTime)
        {
            var jd = dateTime.ToJulianDate();
            return 385001 - 20905 * GetMeanAnomaly(jd).Cos();
        }

        private AzAlt CorrectForRefraction(AzAlt azAlt)
        {
            var alt = azAlt.Altitude + R0 / Math.Tan(azAlt.Altitude.Radians + R1.Radians / (azAlt.Altitude.Radians + R2.Radians));

            return new AzAlt(azAlt.Azimuth, alt);
        }

        private Angle GetMeanAnomaly(JulianDate jd)
        {
            return M0 + M1 * jd.Days;
        }
    }
}