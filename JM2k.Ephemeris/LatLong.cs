﻿using System;

namespace JM2k.Ephemeris
{
    public class LatLong
    {
        private readonly Tuple<Angle, Angle> _point;

        public Angle Latitude => _point.Item1;

        public Angle Longitude => _point.Item2;

        public LatLong(Angle latitude, Angle longitude)
        {
            _point = Tuple.Create(latitude, longitude);
        }

        public override int GetHashCode()
        {
            return _point.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            var azAlt = obj as LatLong;
            if (azAlt == null)
                return false;

            return _point.Equals(azAlt._point);
        }

        public override string ToString()
        {
            var latitude = string.Format("{0} {1}", Latitude, Latitude.Value > 0 ? "North" : "South");
            var longitude = string.Format("{0} {1}", Longitude, Longitude.Value > 0 ? "East" : "West");
            return $"{latitude} | {longitude}";
        }
    }
}