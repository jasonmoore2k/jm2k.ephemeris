﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("JM2k.Ephemeris")]
[assembly: AssemblyDescription("Positions, rise and set times for astronomical objects.")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Jason Moore")]
[assembly: AssemblyProduct("JM2k.Ephemeris")]
[assembly: AssemblyCopyright("Copyright © Jason Moore 2015")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

[assembly: AssemblyVersion("0.0.0.0")]
[assembly: AssemblyFileVersion("0.0.0.0")]
[assembly: ComVisible(false)]
